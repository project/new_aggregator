<?php

/**
 * @file
 *   Administrative form for aggregator.
 */

/**
 * Provides a content-type tab form.
 * Processors and parsers should do form_alter() to provide their settings here
 */
function aggregator_settings_form($form_state) {
  $type = str_replace('-', '_', arg(3));
  $form = array();
  $period = array('-1' => t('none'));
  $period += drupal_map_assoc(array(900, 1800, 3600, 7200, 10800, 21600, 32400, 43200, 64800, 86400, 172800, 259200, 604800, 1209600, 2419200), 'format_interval');
  $parsers = module_implements('aggregator_parse');
  foreach ($parsers as $k => $v) {
    $info = module_invoke($v, 'aggregator_parse', 'info');
    unset($parsers[$k]);
    $parsers[$v] = $info['title'] . ' <span class="description">' . $info['description'] .'</span>';
  }
  $processors = module_implements('aggregator_process');
  foreach ($processors as $k => $v) {
    $info = module_invoke($v, 'aggregator_process', 'info');
    unset($processors[$k]);
    $processors[$v] = $info['title'] . ' <span class="description">' . $info['description'] .'</span>';
  }
  $form['type'] = array('#type' => 'value', '#value' => $type);  // For other modules' form_alter().
  
  if (variable_get('aggregator_feed_' . $type, FALSE)) {
    $form['status'] = array(
      '#type' => 'fieldset',
      '#title' => t('Aggregation functionality for this content type'),
      '#description' => t('Click this button to disable feed aggregation functionality for this content type.'),
      '#weight' => -15,
    );
    $form['status']['disable'] = array(
      '#type' => 'submit',
      '#value' => t('Disable'),
      '#submit' => array('aggregator_settings_form_enable'),
    );
    $form['config'] = array(
      '#type' => 'fieldset',
      '#title' => t('Configuration'),
    );
    $form['config']['aggregator_parser_' . $type] = array(
      '#type' => 'radios',
      '#title' => t('Parser'),
      '#description' => t('Parsers retrieve and parse feed data. Choose one suitable for the type of feeds you would like to aggregate.'),
      '#options' => $parsers,
      '#default_value' => variable_get('aggregator_parser_' . $type, array_pop(array_flip($parsers))),
    );
    $form['config']['aggregator_processor_' . $type] = array(
      '#type' => 'checkboxes',
      '#title' => t('Processors'),
      '#description' => t('Processors act on parsed feed data, for example they store feed items. Pick the processors suitable for your task.'),
      '#options' => $processors,
      '#default_value' => variable_get('aggregator_processor_' . $type, array_slice(array_flip($processors), 0, 1)),
    );
    $form['config']['aggregator_deduper_' . $type] = array(
      '#type' => 'hidden',
      '#value' => variable_get('aggregator_deduper_' . $type, array_pop(variable_get('aggregator_processor_' . $type, array_slice($processors, 0, 1)))),
    );
    $form['config']['aggregator_refresh_' . $type] = array(
      '#type' => 'select',
      '#title' => t('Update interval'),
      '#default_value' => variable_get('aggregator_refresh_' . $type, 3600),
      '#options' => $period,
      '#description' => t('Approximate time between checking feeds of this content type. Requires a correctly configured <a href="@cron">cron maintenance task</a>.', array('@cron' => url('admin/reports/status'))),
    );
    $form['modules'] = array();
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );
  }
  else {
    $form['status'] = array(
      '#type' => 'fieldset',
      '#title' => t('Aggregation functionality for this content type'),
      '#description' => t('Click this button to enable feed aggregation functionality for this content type.'),
      '#weight' => -15,
       );
     $form['status']['enable'] = array(
      '#type' => 'submit',
      '#value' => t('Enable'),
      '#submit' => array('aggregator_settings_form_enable'),
      '#default_value' => TRUE,
      '#weight' => -15,
    );
  }
  return $form;
}

/**
 * Submit handler for enable/disable button.
 */
function aggregator_settings_form_enable($form, &$form_state) {
  if (isset($form_state['values']['disable'])) {
    variable_set('aggregator_feed_'. $form_state['values']['type'], FALSE);
  }
  else if (isset($form_state['values']['enable'])) {
    variable_set('aggregator_feed_'. $form_state['values']['type'], TRUE);
  }
}

/**
 * Stores the values in the {variable} table.
 */
function aggregator_settings_form_submit($form, &$form_state) {
  system_settings_form_submit($form, $form_state);
}
